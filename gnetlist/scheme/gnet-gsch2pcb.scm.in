;;; -*-scheme-*-
;;; $Id$
;;;

;;; gEDA - GNU Electronic Design Automation
;;; gnetlist - GNU Netlist
;;; Copyright (C) 1998-2000 Ales V. Hvezda
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;;  gsch2pcb format  (based on PCBboard format by JM Routoure & Stefan Petersen)
;;  Bill Wilson    billw@wt.net
;;  6/17/2003


;;
;;
(define gsch2pcb:write-top-header
  (lambda (port)
    (display "# release: pcb 1.6.3\n" port)
    (display "PCB(\"\" 6000 5000)\n" port)
    (display "Grid(10 0 0)\n" port)
    (display "Cursor(0 0 3)\n" port)
    (display "Flags(0x000000d0)\n" port)
    (display "Groups(\"1,2,3,s:4,5,6,c:7:8:\")\n" port)
    (display "Styles(\"Signal,10,40,20:Power,25,60,35:Fat,40,60,35:Skinny,8,36,20\")\n" port)))


;;
;;
(define gsch2pcb:write-bottom-footer
  (lambda (port)
    (display "Layer(1 \"solder\")\n(\n)\n" port)
    (display "Layer(2 \"GND-sldr\")\n(\n)\n" port)
    (display "Layer(3 \"Vcc-sldr\")\n(\n)\n" port)
    (display "Layer(4 \"component\")\n(\n)\n" port)
    (display "Layer(5 \"GND-comp\")\n(\n)\n" port)
    (display "Layer(6 \"Vcc-comp\")\n(\n)\n" port)
    (display "Layer(7 \"unused\")\n(\n)\n" port)
    (display "Layer(8 \"unused\")\n(\n)" port)
    (newline port)))

;;
;;

;; Split string at current split-char and returns
;; a pair with substrings. If string is not splitable
;; it returns #f.
(define (string-split the-string split-char)
;;string-index is Guile specific
  (let ((split-index (string-index the-string split-char))
        (last-index (- (string-length the-string) 1)))
;;Check if split-char happens to be in the beginning or end of the string
    (cond ((not split-index)
           #f)
          ((= split-index 0)
           (string-split 
            (substring the-string 1 (string-length the-string)) 
            split-char))
          ((= split-index last-index)
           #f)
          (split-index
           (cons (substring the-string 0 split-index)
                 (substring the-string (+ split-index 1) 
                            (string-length the-string))))
          (else
           #f))))

;; Splits a string with space separated words and returns a list
;; of the words (still as strings).
(define (split-to-list the-string)
  (let ((the-list (string-split the-string #\space)))
    (if the-list
        (cons (car the-list) (split-to-list (cdr the-list)))
        (list the-string))))

;; Joins the elements of a list of strings into a single string,
;; with each element prefixed by a given prefix string.
(define (list-join-with-prefixes the-list prefix)
  (if (null? the-list)
      ""
      (string-append prefix (car the-list)
                     (list-join-with-prefixes (cdr the-list) prefix))))


(define gsch2pcb:write-value-footprint
  (lambda (pipe ls)
    (if (not (null? ls))
;;       refdes contains the first element of ls        
        (let* ((refdes (car ls))
               (value (gnetlist:get-package-attribute refdes "value"))
               (footprint (split-to-list 
                          (gnetlist:get-package-attribute refdes  "footprint") ) ) 
	       (lquote (if gsch2pcb:use-m4 "`" ""))
	       (rquote (if gsch2pcb:use-m4 "'" ""))

	       )

               (display (string-append "PKG_" (car footprint)) pipe)
               (display (string-append "(" lquote (car footprint)) pipe)
               (display (list-join-with-prefixes (cdr footprint) "-") pipe)
               (display (string-append rquote "," lquote refdes rquote "," lquote ) pipe)

               (display value pipe)
               (display (list-join-with-prefixes (cdr footprint) (string-append rquote "," lquote)) pipe)
               (display (string-append rquote ")") pipe)
               (newline pipe)
               (gsch2pcb:write-value-footprint pipe (cdr ls))) )))

;;
;;

(define m4-command "@m4@")
(define m4-pcbdir "@pcbm4dir@")
(define m4-pcbconfdir "@pcbconfdir@")
(define m4-files "")
(define gsch2pcb:use-m4 #f)

;; To emulate popen. Guileish again.
; Needed after guile ver. 1.3.2. To save 1.3a users, wrap it in.
; Doesn't work in guile 1.6 (false-if-exception (use-modules (ice-9 popen)))
(use-modules (ice-9 popen))

(define (gsch2pcb output-filename)
  (let ((port (open-output-file output-filename)))
    (gsch2pcb:write-top-header port)
    (close-port port)
    )

  ;; If we have defined gsch2pcb:use-m4 then run the footprints
  ;; through the pcb m4 setup.  Otherwise skip m4 entirely
 (if gsch2pcb:use-m4
      ;; pipe with the macro define in pcb program
      (let ((pipe (open-output-pipe (string-append
				     m4-command " -d -I. -I" m4-pcbdir " "
				     " -I " m4-pcbconfdir " -I$HOME/.pcb -I. "
				     m4-pcbdir "/common.m4 " m4-files " - >> "
				     output-filename)))
	    )
	
	(display "Using the m4 processor for pcb footprints\n")
	;; packages is a list with the different refdes value
	(gsch2pcb:write-value-footprint pipe packages)
	(close-pipe pipe)
	)
      
      (let ((port  (open output-filename (logior O_WRONLY O_APPEND))))
	(display "Skipping the m4 processor for pcb footprints\n")
	(gsch2pcb:write-value-footprint port packages)
	(close-port port)
	)
      )

  (let ((port (open output-filename (logior O_WRONLY O_APPEND))))
    (gsch2pcb:write-bottom-footer port)
    close-port port)
  )

