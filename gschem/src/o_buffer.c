/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>
#include <stdio.h>

#include <libgeda/libgeda.h>

#include "../include/gschem_struct.h"
#include "../include/globals.h"
#include "../include/prototype.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_copy(GSCHEM_TOPLEVEL *w_current, int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *s_current = NULL;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_copy]\n"));
    return;
  }

  s_current = geda_list_get_glist( toplevel->page_current->selection_list );

  if (object_buffer[buf_num] != NULL) {
    s_delete_object_glist(toplevel, object_buffer[buf_num]);
    object_buffer[buf_num] = NULL;
  }

  toplevel->ADDING_SEL = 1;
  object_buffer[buf_num] =
    o_glist_copy_all_to_glist(toplevel, s_current,
                              object_buffer[buf_num], SELECTION_FLAG);
  toplevel->ADDING_SEL = 0;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_cut(GSCHEM_TOPLEVEL *w_current, int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *s_current = NULL;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_cut]\n"));
    return;
  }

  s_current = geda_list_get_glist( toplevel->page_current->selection_list );

  if (object_buffer[buf_num] != NULL) {
    s_delete_object_glist(toplevel, object_buffer[buf_num]);
    object_buffer[buf_num] = NULL;
  }

  toplevel->ADDING_SEL = 1;
  object_buffer[buf_num] =
    o_glist_copy_all_to_glist(toplevel, s_current,
                              object_buffer[buf_num], SELECTION_FLAG);
  toplevel->ADDING_SEL = 0;
  o_delete(w_current);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_paste_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y,
                          int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  int rleft, rtop, rbottom, rright;
  int x, y;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_paste_start]\n"));
    return;
  }

  if (!world_get_object_glist_bounds(toplevel, object_buffer[buf_num],
                                     &rleft, &rtop,
                                     &rright, &rbottom)) {
    /* If the paste buffer doesn't have any objects
     * to define its any bounds, we drop out here */
    return;
  }

  w_current->first_wx = w_current->second_wx = w_x;
  w_current->first_wy = w_current->second_wy = w_y;
  /* store the buffer number for future use */
  w_current->buffer_number = buf_num;

  /* snap x and y to the grid, pointed out by Martin Benes */
  x = snap_grid(toplevel, rleft);
  y = snap_grid(toplevel, rtop);

  toplevel->ADDING_SEL = 1;
  o_glist_translate_world(toplevel, -x, -y, object_buffer[buf_num]);
  toplevel->ADDING_SEL = 0;

  toplevel->ADDING_SEL = 1;
  o_glist_translate_world(toplevel, w_x, w_y, object_buffer[buf_num]);
  toplevel->ADDING_SEL = 0;

  w_current->event_state = ENDPASTE;

  o_buffer_paste_rubberpaste_xor (w_current, buf_num, TRUE);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_paste_end(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y,
                        int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  int w_diff_x, w_diff_y;
  OBJECT *o_current;
  OBJECT *o_saved;
  SELECTION *temp_list = o_selection_new();
  PAGE *p_current;
  GList *connected_objects = NULL;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_paste_end]\n"));
    return;
  }

  /* erase old image */
  o_buffer_paste_rubberpaste_xor (w_current, buf_num, FALSE);

  /* calc and translate objects to their final position */
  w_diff_x = w_current->second_wx - w_current->first_wx;
  w_diff_y = w_current->second_wy - w_current->first_wy;

#if DEBUG
  printf("%d %d\n", w_diff_x, w_diff_y);
#endif

  toplevel->ADDING_SEL = 1;
  o_glist_translate_world(toplevel, w_diff_x, w_diff_y,
                          object_buffer[buf_num]);
  toplevel->ADDING_SEL = 0;

  o_current = object_buffer[buf_num]->data;
  p_current = toplevel->page_current;

  o_saved = p_current->object_tail;
  o_list_copy_all(toplevel, o_current, p_current->object_tail,
                  NORMAL_FLAG);

  p_current->object_tail = return_tail(p_current->object_head);
  o_current = o_saved->next;

  /* now add new objects to the selection list */
  while (o_current != NULL) {
    o_selection_add( temp_list, o_current );
    s_conn_update_object(toplevel, o_current);
    if (o_current->type == OBJ_COMPLEX || o_current->type == OBJ_PLACEHOLDER) {
      connected_objects = s_conn_return_complex_others(
                                                       connected_objects,
                                                       o_current);
    } else {
      connected_objects = s_conn_return_others(connected_objects,
                                               o_current);
    }
    o_current = o_current->next;
  }

  o_cue_redraw_all(w_current, o_saved->next, TRUE);
  o_cue_undraw_list(w_current, connected_objects);
  o_cue_draw_list(w_current, connected_objects);
  g_list_free(connected_objects);
  connected_objects = NULL;

  o_select_unselect_all( w_current );
  geda_list_add_glist( toplevel->page_current->selection_list, geda_list_get_glist( temp_list ) );

  g_object_unref( temp_list );

  toplevel->page_current->CHANGED = 1;
  o_redraw(w_current, o_saved->next, TRUE); /* only redraw new objects */
  o_undo_savestate(w_current, UNDO_ALL);
  i_update_menus(w_current);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_paste_rubberpaste (GSCHEM_TOPLEVEL *w_current, int buf_num,
                                 int w_x, int w_y)
{
  o_buffer_paste_rubberpaste_xor (w_current, buf_num, FALSE);
  w_current->second_wx = w_x;
  w_current->second_wy = w_y;
  o_buffer_paste_rubberpaste_xor (w_current, buf_num, TRUE);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_paste_rubberpaste_xor(GSCHEM_TOPLEVEL *w_current, int buf_num,
                                    int drawing)
{
  o_drawbounding(w_current, object_buffer[buf_num],
                 x_get_darkcolor(w_current->bb_color), drawing);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_init(void)
{
  int i;

  for (i = 0 ; i < MAX_BUFFERS; i++) {
    object_buffer[i] = NULL;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_free(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  int i;

  for (i = 0 ; i < MAX_BUFFERS; i++) {
    if (object_buffer[i]) {
      s_delete_object_glist(toplevel, object_buffer[i]);
      object_buffer[i] = NULL;
    }
  }
}
