
extern int default_init_right;
extern int default_init_bottom;

extern char *default_series_name;
extern char *default_untitled_name;
extern char *default_font_directory;
extern char *default_scheme_directory;
extern char *default_bitmap_directory;
extern char *default_bus_ripper_symname;
extern char *default_postscript_prolog;

extern char *default_always_promote_attributes;

extern char *default_print_command;

extern int default_attribute_promotion;
extern int default_promote_invisible;
extern int default_keep_invisible;

