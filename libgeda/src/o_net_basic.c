/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  \param [in]  toplevel  The TOPLEVEL object.
 *  \param [in]  line
 *  \param [out] left
 *  \param [out] top
 *  \param [out] right
 *  \param [out] bottom
 */
void world_get_net_bounds(TOPLEVEL *toplevel, OBJECT *object, int *left,
                          int *top, int *right, int *bottom)
{
  world_get_line_bounds( toplevel, object, left, top, right, bottom );
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in,out] object_list
 *  \param [in]     type
 *  \param [in]     color
 *  \param [in]     x1
 *  \param [in]     y1
 *  \param [in]     x2
 *  \param [in]     y2
 *  \return OBJECT *
 */
OBJECT *o_net_add(TOPLEVEL *toplevel, OBJECT *object_list, char type,
		  int color, int x1, int y1, int x2, int y2)
{
  int left, right, top, bottom;
  OBJECT *new_node;

  new_node = s_basic_init_object("net");
  new_node->type = type;
  new_node->color = color;

  new_node->line = (LINE *) g_malloc(sizeof(LINE));
  /* check for null */

  new_node->line->x[0] = x1;
  new_node->line->y[0] = y1;
  new_node->line->x[1] = x2;
  new_node->line->y[1] = y2;
  new_node->line_width = NET_WIDTH;

  world_get_net_bounds(toplevel, new_node, &left, &top, &right,
                 &bottom);

  new_node->w_left = left;
  new_node->w_top = top;
  new_node->w_right = right;
  new_node->w_bottom = bottom;

  new_node->draw_func = net_draw_func;
  new_node->sel_func = select_func;

  object_list = (OBJECT *) s_basic_link_object(new_node, object_list);


  if (!toplevel->ADDING_SEL) {
    s_tile_add_object(toplevel, object_list,
		      new_node->line->x[0], new_node->line->y[0],
		      new_node->line->x[1], new_node->line->y[1]);
    s_conn_update_object(toplevel, object_list);
  }

  return (object_list);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_recalc(TOPLEVEL *toplevel, OBJECT *o_current)
{
  int left, right, top, bottom;

  if (o_current == NULL) {
    return;
  }

  if (o_current->line == NULL) {
    return;
  }

  world_get_net_bounds(toplevel, o_current, &left, &top, &right,
                 &bottom);

  o_current->w_left = left;
  o_current->w_top = top;
  o_current->w_right = right;
  o_current->w_bottom = bottom;


}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
OBJECT *o_net_read(TOPLEVEL *toplevel, OBJECT *object_list, char buf[],
		   unsigned int release_ver, unsigned int fileformat_ver)
{
  char type;
  int x1, y1;
  int x2, y2;
  int d_x1, d_y1;
  int d_x2, d_y2;
  int color;

  sscanf(buf, "%c %d %d %d %d %d\n", &type, &x1, &y1, &x2, &y2, &color);
  d_x1 = x1;
  d_y1 = y1;
  d_x2 = x2;
  d_y2 = y2;

  if (x1 == x2 && y1 == y2) {
    s_log_message(_("Found a zero length net [ %c %d %d %d %d %d ]\n"),
                  type, x1, y1, x2, y2, color);
  }


  if (toplevel->override_net_color != -1) {
    color = toplevel->override_net_color;
  }

  if (color < 0 || color > MAX_COLORS) {
    s_log_message(_("Found an invalid color [ %s ]\n"), buf);
    s_log_message(_("Setting color to WHITE\n"));
    color = WHITE;
  }

  object_list =
  o_net_add(toplevel, object_list, type, color, d_x1, d_y1, d_x2,
            d_y2);
  return (object_list);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
char *o_net_save(OBJECT *object)
{
  int x1, x2, y1, y2;
  int color;
  char *buf;

  x1 = object->line->x[0];
  y1 = object->line->y[0];
  x2 = object->line->x[1];
  y2 = object->line->y[1];

  /* Use the right color */
  if (object->saved_color == -1) {
    color = object->color;
  } else {
    color = object->saved_color;
  }

  buf = g_strdup_printf("%c %d %d %d %d %d", object->type, x1, y1, x2, y2, color);
  return (buf);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_translate_world(TOPLEVEL *toplevel, int x1, int y1,
			   OBJECT *object)
{
  int left, right, top, bottom;

  if (object == NULL)
  printf("ntw NO!\n");


  /* Update world coords */
  object->line->x[0] = object->line->x[0] + x1;
  object->line->y[0] = object->line->y[0] + y1;
  object->line->x[1] = object->line->x[1] + x1;
  object->line->y[1] = object->line->y[1] + y1;

  /* Update bounding box */
  world_get_net_bounds(toplevel, object, &left, &top, &right, &bottom);

  object->w_left = left;
  object->w_top = top;
  object->w_right = right;
  object->w_bottom = bottom;

  s_tile_update_object(toplevel, object);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
OBJECT *o_net_copy(TOPLEVEL *toplevel, OBJECT *list_tail,
		   OBJECT *o_current)
{
  OBJECT *new_obj;
  int color;

  if (o_current->saved_color == -1) {
    color = o_current->color;
  } else {
    color = o_current->saved_color;
  }

  /* make sure you fix this in pin and bus as well */
  /* still doesn't work... you need to pass in the new values */
  /* or don't update and update later */
  /* I think for now I'll disable the update and manually update */
  new_obj = o_net_add(toplevel, list_tail, OBJ_NET, color,
                      o_current->line->x[0], o_current->line->y[0],
                      o_current->line->x[1], o_current->line->y[1]);

  new_obj->line->x[0] = o_current->line->x[0];
  new_obj->line->y[0] = o_current->line->y[0];
  new_obj->line->x[1] = o_current->line->x[1];
  new_obj->line->y[1] = o_current->line->y[1];

  return (new_obj);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current,
		 int origin_x, int origin_y)
{
  int offset, offset2;
  int cross, net_width;
  int x1, y1;
  int x2, y2;

  if (o_current == NULL) {
    printf("got null in o_net_print\n");
    return;
  }

  offset = 7 * 6;
  offset2 = 7;

  cross = offset;

  if (toplevel->print_color) {
    f_print_set_color(fp, o_current->color);
  }

  net_width = 2;
  if (toplevel->net_style == THICK) {
    net_width = NET_WIDTH;
  }

  x1 = o_current->line->x[0] - origin_x,
  y1 = o_current->line->y[0] - origin_y;
  x2 = o_current->line->x[1] - origin_x,
  y2 = o_current->line->y[1] - origin_y;

  fprintf(fp, "%d %d %d %d %d line\n", x1,y1,x2,y2,net_width);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_rotate_world(TOPLEVEL *toplevel,
			int world_centerx, int world_centery, int angle,
			OBJECT *object)
{
  int newx, newy;

  if (angle == 0)
  return;

  /* translate object to origin */
  o_net_translate_world(toplevel, -world_centerx, -world_centery,
                        object);

  rotate_point_90(object->line->x[0], object->line->y[0], angle,
                  &newx, &newy);

  object->line->x[0] = newx;
  object->line->y[0] = newy;

  rotate_point_90(object->line->x[1], object->line->y[1], angle,
                  &newx, &newy);

  object->line->x[1] = newx;
  object->line->y[1] = newy;

  o_net_translate_world(toplevel, world_centerx, world_centery, object);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_mirror_world(TOPLEVEL *toplevel, int world_centerx,
			int world_centery, OBJECT *object)
{
  /* translate object to origin */
  o_net_translate_world(toplevel, -world_centerx, -world_centery,
                        object);

  object->line->x[0] = -object->line->x[0];

  object->line->x[1] = -object->line->x[1];

  o_net_translate_world(toplevel, world_centerx, world_centery, object);
}

int o_net_orientation(OBJECT *object)
{
    if (object->line->y[0] == object->line->y[1]) {
	return (HORIZONTAL);
    }

    if (object->line->x[0] == object->line->x[1]) {
	return (VERTICAL);
    }

    return (NEITHER);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 * this function does the actual work of making one net segment out of two
 * connected segments
 * The second object (del_object) is the object that should be deleted
 */
void o_net_consolidate_lowlevel(OBJECT *object, OBJECT *del_object,
				int orient)
{
  int temp1, temp2;
  int final1, final2;
  int changed = 0;
  GList *a_iter;
  ATTRIB *a_current;

#if DEBUG
  printf("o %d %d %d %d\n", object->line->x[0], object->line->y[0],
         object->line->x[1], object->line->y[1]);
  printf("d %d %d %d %d\n", del_object->line->x[0],
         del_object->line->y[0], del_object->line->x[1],
         del_object->line->y[1]);
#endif


  if (orient == HORIZONTAL) {

    temp1 = min(object->line->x[0], del_object->line->x[0]);
    temp2 = min(object->line->x[1], del_object->line->x[1]);

    final1 = min(temp1, temp2);

    temp1 = max(object->line->x[0], del_object->line->x[0]);
    temp2 = max(object->line->x[1], del_object->line->x[1]);

    final2 = max(temp1, temp2);

    object->line->x[0] = final1;
    object->line->x[1] = final2;
    changed = 1;
  }

  if (orient == VERTICAL) {
    temp1 = min(object->line->y[0], del_object->line->y[0]);
    temp2 = min(object->line->y[1], del_object->line->y[1]);

    final1 = min(temp1, temp2);

    temp1 = max(object->line->y[0], del_object->line->y[0]);
    temp2 = max(object->line->y[1], del_object->line->y[1]);

    final2 = max(temp1, temp2);

    object->line->y[0] = final1;
    object->line->y[1] = final2;
    changed = 1;
  }
#if DEBUG
  printf("fo %d %d %d %d\n", object->line->x[0], object->line->y[0],
         object->line->x[1], object->line->y[1]);
#endif

  /* Move any attributes from the deleted object*/
  if (changed && del_object->attribs != NULL) {

    /* Reassign the attached_to pointer on attributes from the del object */
    a_iter = del_object->attribs;
    while (a_iter != NULL) {
      a_current = a_iter->data;
      a_current->object->attached_to = object;
      a_iter = g_list_next (a_iter);
    }

    object->attribs = g_list_concat (object->attribs, del_object->attribs);

    /* Don't free del_object->attribs as it's relinked into object's list */
    del_object->attribs = NULL;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 * check to see if this connection also causes a midpoint
 * if so, return false, else return true
 */
int o_net_consolidate_nomidpoint(OBJECT *object, int x, int y)
{
  GList *c_current;
  CONN *conn;

  c_current = object->conn_list;
  while(c_current != NULL) {
    conn = (CONN *) c_current->data;
    if (conn->other_object) {
      if (conn->other_object->sid != object->sid &&
          conn->x == x && conn->y == y &&
          conn->type == CONN_MIDPOINT) {
#if DEBUG        
        printf("Found one! %s\n", conn->other_object->name); 
#endif         
        return(FALSE);
      }
    }
    
    c_current = g_list_next(c_current);
  }

  return(TRUE);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
int o_net_consolidate_segments(TOPLEVEL *toplevel, OBJECT *object)
{
  int object_orient;
  int other_orient;
  GList *c_current;
  CONN *conn;
  OBJECT *other_object;
  int changed = 0;
  int reselect_new=FALSE;

  if (object == NULL) {
    return(0);
  }

  if (object->type != OBJ_NET) {
    return(0);
  }

  object_orient = o_net_orientation(object);

  c_current = object->conn_list;
  while(c_current != NULL) {
    conn = (CONN *) c_current->data;
    other_object = conn->other_object;

    /* only look at end points which have a valid end on the other side */
    if (other_object != NULL && conn->type == CONN_ENDPOINT &&
        conn->other_whichone != -1 && conn->whichone != -1 &&
        o_net_consolidate_nomidpoint(object, conn->x, conn->y) ) {

      if (other_object->type == OBJ_NET) {
        other_orient = o_net_orientation(other_object);

        /* - both objects have the same orientation (either vert or horiz) */
        /* - it's not the same object */
        if (object_orient == other_orient &&
            object->sid != other_object->sid &&
            other_orient != NEITHER) {

#if DEBUG
          printf("consolidating %s to %s\n", object->name, other_object->name);
#endif

          o_net_consolidate_lowlevel(object, other_object, other_orient);

          changed++;
          if (other_object->selected == TRUE ) {
            o_selection_remove( toplevel->page_current->selection_list, other_object );
            reselect_new=TRUE;
          }

          if (reselect_new == TRUE) {
            o_selection_remove( toplevel->page_current->selection_list, object );
            o_selection_add( toplevel->page_current->selection_list, object );
          }

          s_conn_remove(toplevel, other_object);
          s_delete(toplevel, other_object);
          o_net_recalc(toplevel, object);
          s_tile_update_object(toplevel, object);
          s_conn_update_object(toplevel, object);
          toplevel->page_current->object_tail =
            return_tail(toplevel->page_current->object_head);
          return(-1);
        }
      }
      
    }

    c_current = g_list_next (c_current);
  }

  return(0);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_consolidate(TOPLEVEL *toplevel)
{
  OBJECT *o_current;
  int status = 0;

  o_current = toplevel->page_current->object_head;

  while (o_current != NULL) {

    if (o_current->type == OBJ_NET) {
      status = o_net_consolidate_segments(toplevel, o_current);
    }

    if (status == -1) {
      o_current = toplevel->page_current->object_head;
      status = 0;
    } else {
      o_current = o_current->next;
    }
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *
 */
void o_net_modify(TOPLEVEL *toplevel, OBJECT *object,
		  int x, int y, int whichone)
{
  int left, right, top, bottom;

  object->line->x[whichone] = x;
  object->line->y[whichone] = y;

  world_get_net_bounds(toplevel, object, &left, &top, &right, &bottom);

  object->w_left = left;
  object->w_top = top;
  object->w_right = right;
  object->w_bottom = bottom;

  s_tile_update_object(toplevel, object);
}
